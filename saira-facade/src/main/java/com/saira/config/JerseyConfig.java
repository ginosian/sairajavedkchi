package com.saira.config;

import com.saira.endpoint.impl.SairaEndpointImpl;
import com.saira.endpoint.impl.SairaInfoEndpointImpl;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;


@Component
@ApplicationPath("/v1")
public class JerseyConfig extends AbstractJerseyConfig {

    public JerseyConfig() {
        super();
        registerEndpoints();
    }

    private void registerEndpoints() {
        register(SairaInfoEndpointImpl.class);
        register(SairaEndpointImpl.class);
    }
}
