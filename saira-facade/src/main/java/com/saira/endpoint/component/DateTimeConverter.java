package com.saira.endpoint.component;

import com.saira.error.ApiException;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateTimeConverter {

    private static SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
    private static SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MMM-yyyy");
    private static SimpleDateFormat formatter3 = new SimpleDateFormat("MM dd, yyyy");
    private static SimpleDateFormat formatter4 = new SimpleDateFormat("E, MMM dd yyyy");
    private static SimpleDateFormat formatter5 = new SimpleDateFormat("E, MMM dd yyyy HH:mm:ss");
    private static SimpleDateFormat formatter6 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

    public Date convert(final String date){
        Date result = try6(date);
        result = result == null ? try5(date) : result;
        result = result == null ? try4(date) : result;
        result = result == null ? try3(date) : result;
        result = result == null ? try2(date) : result;
        result = result == null ? try1(date) : result;
        if(result == null){
            throw new ApiException("Could not convert the date", 400);
        }
        return result;
    }

    private Date try1(final String date){
        Date result;
        try{
            return formatter1.parse(date);
        } catch (Exception e){
            return null;
        }
    }

    private Date try2(final String date){
        Date result;
        try{
            return formatter2.parse(date);
        } catch (Exception e){
            return null;
        }
    }

    private Date try3(final String date){
        Date result;
        try{
            return formatter3.parse(date);
        } catch (Exception e){
            return null;
        }
    }

    private Date try4(final String date){
        Date result;
        try{
            return formatter4.parse(date);
        } catch (Exception e){
            return null;
        }
    }

    private Date try5(final String date){
        Date result;
        try{
            return formatter5.parse(date);
        } catch (Exception e){
            return null;
        }
    }

    private Date try6(final String date){
        Date result;
        try{
            return formatter6.parse(date);
        } catch (Exception e){
            return null;
        }
    }
}
