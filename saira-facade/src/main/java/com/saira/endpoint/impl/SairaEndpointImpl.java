package com.saira.endpoint.impl;

import com.saira.ListResponseDTO;
import com.saira.ResponseDTO;
import com.saira.dto.person.PersonCreationDTO;
import com.saira.dto.person.PersonResponseDTO;
import com.saira.dto.person.PersonUpdateDTO;
import com.saira.endpoint.ExceptionHelper;
import com.saira.endpoint.SairaEndpoint;
import com.saira.endpoint.component.DateTimeConverter;
import com.saira.enums.SortField;
import com.saira.mapper.BeanMapper;
import com.saira.service.PersonService;
import com.saira.service.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

public class SairaEndpointImpl implements SairaEndpoint {

    @Autowired
    private PersonService personService;

    @Autowired
    private BeanMapper beanMapper;

    @Autowired
    private ExceptionHelper exceptionHelper;

    @Autowired
    private DateTimeConverter dateTimeConverter;

    @Override
    public ResponseDTO<PersonResponseDTO> get(final Long personId) {
        try {
            final PersonResponse person = personService.get(personId);
            return new ResponseDTO<>(null, beanMapper.map(person, PersonResponseDTO.class));
        } catch (Exception e){
            return new ResponseDTO<>(exceptionHelper.resolve(e, 401), null);
        }
    }

    @Override
    public ListResponseDTO<PersonResponseDTO> getAll(int page, int size, SortField sortBy, String sortingOrder) {
        try {
            final PersonSearchResponse response = personService.getList(new SortingAndPagingMetaData(sortBy, sortingOrder, page, size));
            final List<PersonResponse> persons = response.getPersons();
            final SortingAndPagingMetaData metaData = response.getSortingAndPagingMetaData();
            return new ListResponseDTO<>(null, persons.size(), metaData.getPage(), metaData.getSize(), beanMapper.mapAsList(persons, PersonResponseDTO.class));
        } catch (Exception e){
            return new ListResponseDTO<>(exceptionHelper.resolve(e, 401), 0, 0, 0, null);
        }
    }

    @Override
    public ListResponseDTO<PersonResponseDTO> search(String name, String creationDate, int page, int size, SortField sortBy, String sortingOrder) {
        try {
            SortingAndPagingMetaData metaData = new SortingAndPagingMetaData(sortBy, sortingOrder, page, size);
            final Date creation = StringUtils.isEmpty(creationDate) ? null : dateTimeConverter.convert(creationDate);
            final PersonSearchResponse response = personService.search(new PersonSearchRequest(name, creation, metaData));
            final List<PersonResponse> persons = response.getPersons();
            metaData = response.getSortingAndPagingMetaData();
            return new ListResponseDTO<>(null, persons.size(), metaData.getPage(), metaData.getSize(), beanMapper.mapAsList(persons, PersonResponseDTO.class));
        } catch (Exception e){
            return new ListResponseDTO<>(exceptionHelper.resolve(e, 401), 0, 0, 0, null);
        }
    }

    @Override
    public ResponseDTO<PersonResponseDTO> create(final PersonCreationDTO request) {
        try {
            final PersonCreationRequest requestToService = beanMapper.map(request, PersonCreationRequest.class);
            final PersonResponse person = personService.create(requestToService);
            return new ResponseDTO<>(null, beanMapper.map(person, PersonResponseDTO.class));
        } catch (Exception e){
            return new ResponseDTO<>(exceptionHelper.resolve(e, 501), null);
        }
    }

    @Override
    public ResponseDTO<PersonResponseDTO> update(final PersonUpdateDTO request) {
        try {
            final PersonUpdateRequest requestToService = beanMapper.map(request, PersonUpdateRequest.class);
            final PersonResponse person = personService.update(requestToService);
            return new ResponseDTO<>(null, beanMapper.map(person, PersonResponseDTO.class));
        } catch (Exception e){
            return new ResponseDTO<>(exceptionHelper.resolve(e, 501), null);
        }
    }

    @Override
    public ResponseDTO<PersonResponseDTO> partialUpdate(final PersonUpdateDTO request) {
        try {
            final PersonUpdateRequest requestToService = beanMapper.map(request, PersonUpdateRequest.class);
            final PersonResponse person = personService.partialUpdate(requestToService);
            return new ResponseDTO<>(null, beanMapper.map(person, PersonResponseDTO.class));
        } catch (Exception e){
            return new ResponseDTO<>(exceptionHelper.resolve(e, 501), null);
        }
    }

    @Override
    public ResponseDTO<String> delete(final Long personId) {
        try {
            personService.delete(personId);
            return new ResponseDTO<>(null, "Person successfully deleted.");
        } catch (Exception e){
            return new ResponseDTO<>(exceptionHelper.resolve(e, 501), null);
        }
    }
}
