package com.saira.endpoint.impl;

import com.saira.dto.info.InfoDTO;
import com.saira.endpoint.SairaInfoEndpoint;

public class SairaInfoEndpointImpl implements SairaInfoEndpoint {

    @Override
    public InfoDTO get() {
        return new InfoDTO("This is Saira micro-service. It works just great");
    }
}
