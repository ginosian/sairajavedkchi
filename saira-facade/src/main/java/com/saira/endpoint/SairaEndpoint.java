package com.saira.endpoint;


import com.saira.ListResponseDTO;
import com.saira.ResponseDTO;
import com.saira.dto.person.PersonCreationDTO;
import com.saira.dto.person.PersonResponseDTO;
import com.saira.dto.person.PersonUpdateDTO;
import com.saira.enums.SortField;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/persons")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface SairaEndpoint {

    @GET
    @Path("/{personId}")
    ResponseDTO<PersonResponseDTO> get(@PathParam("personId") Long personId);

    @GET
    @Path("")
    ListResponseDTO<PersonResponseDTO> getAll(
            @QueryParam("page") int page,
            @QueryParam("size") int size,
            @QueryParam("sortBy") SortField sortBy,
            @QueryParam("sortingOrder") String sortingOrder);

    @GET
    @Path("/search")
    ListResponseDTO<PersonResponseDTO> search(
            @QueryParam("name") String name,
            @QueryParam("creationDate") String creationDate,
            @QueryParam("page") int page,
            @QueryParam("size") int size,
            @QueryParam("sortBy") SortField sortBy,
            @QueryParam("sortingOrder") String sortingOrder);

    @POST
    @Path("")
    ResponseDTO<PersonResponseDTO> create(@NotNull PersonCreationDTO request);

    @PUT
    @Path("")
    ResponseDTO<PersonResponseDTO> update(@NotNull PersonUpdateDTO request);

    @PATCH
    @Path("")
    ResponseDTO<PersonResponseDTO> partialUpdate(@NotNull PersonUpdateDTO request);

    @DELETE
    @Path("/{personId}")
    ResponseDTO<String> delete(@PathParam("personId") Long personId);

}
