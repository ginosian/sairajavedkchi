package com.saira.repository;

import com.saira.entity.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface PersonJPARepository extends JpaRepository<Person, Long> {
    Page<Person> findAll(Pageable pageable);
    Page<Person> findByNameAndCreated(String name, Date created, Pageable pageable);
    Page<Person> findByNameOrCreated(String name, Date created, Pageable pageable);
    Page<Person> findByCreated(Date created, Pageable pageable);
    Page<Person> findByName(String name, Pageable pageable);
}
