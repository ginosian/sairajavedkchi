package com.saira.service.impl;

import com.saira.entity.Person;
import com.saira.repository.PersonJPARepository;
import com.saira.service.PersonService;
import com.saira.service.component.PersonConverter;
import com.saira.service.component.SortingAndPagingResolverStrategy;
import com.saira.service.model.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.Assert.hasText;
import static org.springframework.util.Assert.notNull;

@Service
public class PersonServiceImpl implements PersonService {

    private static final Logger logger = LoggerFactory.getLogger(PersonServiceImpl.class);

    @Autowired
    private PersonJPARepository personJPARepository;

    @Autowired
    private PersonConverter personConverter;

    @Autowired
    private SortingAndPagingResolverStrategy sortingAndPagingResolverStrategy;

    @Override
    public PersonResponse get(Long personId) {
        notNull(personId, "Id can not be null.");
        logger.trace("Getting persons with id: '{}'...", personId);
        final Person person = personJPARepository.getOne(personId);
        final PersonResponse personResponse = personConverter.convert(person);
        logger.debug("Done getting persons with id: '{}'.", person.getId());
        return personResponse;
    }

    @Override
    public PersonSearchResponse getList(final SortingAndPagingMetaData metaData) {
        logger.trace("Getting persons...");
        final SortingAndPagingMetaData data = sortingAndPagingResolverStrategy.resolve(metaData);
        final PageRequest page = new PageRequest(data.getPage(), data.getSize(),Sort.Direction.DESC, data.getSortBy().getFiledName());
        final Page<Person> personList = personJPARepository.findAll(page);
        logger.debug("Done getting persons");

        final List<PersonResponse> persons = personList.stream().map(person -> personConverter.convert(person)).collect(Collectors.toList());
        final PersonSearchResponse personSearchResponse = new PersonSearchResponse();
        personSearchResponse.setPersons(persons);
        personSearchResponse.setSortingAndPagingMetaData(data);
        return personSearchResponse;
    }

    @Override
    public PersonSearchResponse search(final PersonSearchRequest request) {
        notNull(request, "request can not be null.");
        logger.trace("Searching for persons...");
        final SortingAndPagingMetaData data = sortingAndPagingResolverStrategy.resolve(request.getSortingAndPagingMetaData());
        final String name = request.getName();
        final Date created = request.getCreated();
        Page<Person> personList;
        final PageRequest page = new PageRequest(data.getPage(), data.getSize(),Sort.Direction.DESC,data.getSortBy().getFiledName());
        if(StringUtils.isNotEmpty(name) && created != null){
            personList = personJPARepository.findByNameAndCreated(name, created, page);
            if(personList == null){
                personList = personJPARepository.findByNameOrCreated(name, created, page);
            }
        } else if(StringUtils.isNotEmpty(name) && created == null){
            personList = personJPARepository.findByName(name, page);
        } else {
            personList = personJPARepository.findByCreated(created, page);
        }
        logger.debug("Done searching for persons");
        final List<PersonResponse> persons = personList.stream().map(person -> personConverter.convert(person)).collect(Collectors.toList());
        final PersonSearchResponse personSearchResponse = new PersonSearchResponse();
        personSearchResponse.setPersons(persons);
        personSearchResponse.setSortingAndPagingMetaData(data);
        return personSearchResponse;
    }

    @Transactional
    @Override
    public PersonResponse create(final PersonCreationRequest request) {
        notNull(request, "request can not be null.");
        final String name = request.getName();
        hasText(name, "name can not be null or empty");
        Person person = new Person();
        person.setName(name);
        logger.trace("Creating person...");
        person = personJPARepository.save(person);
        logger.debug("Done creating person id:'{}'", person.getId());
        return personConverter.convert(person);
    }

    @Transactional
    @Override
    public PersonResponse update(PersonUpdateRequest request) {
        notNull(request, "request can not be null.");
        final Long id = request.getId();
        notNull(id, "id can not be null.");
        final Person person = personJPARepository.getOne(id);
        final String name = request.getName();
        if(StringUtils.isNotEmpty(name)){
            person.setName(name);
        }
        return personConverter.convert(personJPARepository.save(person));
    }

    @Transactional
    @Override
    public PersonResponse partialUpdate(PersonUpdateRequest request) {
        notNull(request, "request can not be null.");
        final Long id = request.getId();
        notNull(id, "id can not be null.");
        logger.trace("Creating person id:'{}'...", id);
        final Person person = personJPARepository.getOne(id);
        final String name = request.getName();
        if(StringUtils.isNotEmpty(name)){
            person.setName(name);
        }
        logger.debug("Done creating person id:'{}'", person.getId());
        return personConverter.convert(personJPARepository.save(person));
    }

    @Transactional
    @Override
    public void delete(Long id) {
        notNull(id, "id can not be null.");
        logger.trace("Deleting person id:'{}'...", id);
        final Person person = personJPARepository.getOne(id);
        personJPARepository.delete(person);
        logger.debug("Done deleting person id:'{}'", person.getId());
    }
}
