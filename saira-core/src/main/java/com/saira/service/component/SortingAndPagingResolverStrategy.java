package com.saira.service.component;

import com.saira.enums.SortField;
import com.saira.service.model.SortingAndPagingMetaData;
import org.springframework.stereotype.Component;

@Component
public class SortingAndPagingResolverStrategy {

    public SortingAndPagingMetaData resolve(final SortingAndPagingMetaData metaData){
        int page = 0;
        int size = 0;
        SortField sortField = null;
        String sortDirection = null;
        if (metaData != null) {
            page = metaData.getPage();
            size = metaData.getSize();
            sortField = metaData.getSortBy();
            sortDirection = metaData.getSortDirection();
        }
        size = size == 0 ? 3 : size;
        sortField = sortField == null ? SortField.NAME : sortField;
        sortDirection = sortDirection == null ? "DESC" : sortDirection;
        return new SortingAndPagingMetaData(sortField, sortDirection, page, size);
    }
}
