package com.saira.service.component;

import com.saira.entity.Person;
import com.saira.service.model.PersonResponse;
import org.springframework.stereotype.Component;

@Component
public class PersonConverter {

    public PersonResponse convert(final Person person){
        final PersonResponse response = new PersonResponse();
        response.setId(person.getId());
        response.setName(person.getName());
        response.setCreationDate(person.getCreated());
        return response;
    }
}
