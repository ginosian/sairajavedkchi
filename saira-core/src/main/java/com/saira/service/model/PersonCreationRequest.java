package com.saira.service.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonCreationRequest {
    private String name;
}
