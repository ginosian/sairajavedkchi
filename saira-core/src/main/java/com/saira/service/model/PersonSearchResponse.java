package com.saira.service.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PersonSearchResponse {
    private SortingAndPagingMetaData sortingAndPagingMetaData;
    private List<PersonResponse> persons;
}
