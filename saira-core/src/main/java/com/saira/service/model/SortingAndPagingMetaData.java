package com.saira.service.model;

import com.saira.enums.SortField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SortingAndPagingMetaData {
    private SortField sortBy;
    private String sortDirection;
    private int page;
    private int size;
}
