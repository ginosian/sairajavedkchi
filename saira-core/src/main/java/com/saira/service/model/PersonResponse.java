package com.saira.service.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class PersonResponse {
    private Long id;
    private String name;
    private Date creationDate;
}
