package com.saira.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PersonSearchRequest {
    private String name;
    private Date created;
    private SortingAndPagingMetaData sortingAndPagingMetaData;
}
