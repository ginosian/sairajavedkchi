package com.saira.service;

import com.saira.service.model.*;

public interface PersonService {
    PersonResponse get(Long personId);
    PersonSearchResponse getList(SortingAndPagingMetaData metaData);
    PersonSearchResponse search(PersonSearchRequest request);
    PersonResponse create(PersonCreationRequest request);
    PersonResponse update(PersonUpdateRequest request);
    PersonResponse partialUpdate(PersonUpdateRequest request);
    void delete(Long id);
}
