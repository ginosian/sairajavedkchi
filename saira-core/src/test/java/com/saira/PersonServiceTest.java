package com.saira;


import com.saira.enums.SortField;
import com.saira.service.PersonService;
import com.saira.service.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

import static io.github.benas.randombeans.api.EnhancedRandom.random;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Transactional
public class PersonServiceTest {

    @Autowired
    private PersonService personService;

    @Test
    public void getSuccessful(){
        final PersonCreationRequest request = random(PersonCreationRequest.class);
        final PersonResponse response = personService.create(request);
        final PersonResponse result = personService.get(response.getId());
        assertNotNull(result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getFailure(){
        final PersonCreationRequest request = random(PersonCreationRequest.class);
        final PersonResponse response = personService.create(request);
        final PersonResponse result = personService.get(response.getId() + 589);
        assertNotNull(result);
    }

    @Test
    public void testCreationSuccessful(){
        final PersonCreationRequest request = random(PersonCreationRequest.class);
        final PersonResponse response = personService.create(request);
        assertNotNull(response);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFailedMissingName(){
        final PersonCreationRequest request = random(PersonCreationRequest.class);
        request.setName(null);
        personService.create(request);
    }


    @Test
    public void updateSuccessful(){
        final PersonCreationRequest request = random(PersonCreationRequest.class);
        final PersonResponse response = personService.create(request);
        PersonResponse result = personService.get(response.getId());
        final PersonUpdateRequest personUpdateRequest = new PersonUpdateRequest(result.getId(), "some other name");
        result = personService.update(personUpdateRequest);
        assertEquals(personUpdateRequest.getName(), result.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFailedMissingId(){
        final PersonUpdateRequest request = random(PersonUpdateRequest.class);
        request.setId(null);
        personService.update(request);
    }

    @Test
    public void testSearch(){
        final PersonCreationRequest request1 = random(PersonCreationRequest.class);
        personService.create(request1);

        final PersonCreationRequest request2 = random(PersonCreationRequest.class);
        personService.create(request2);

        final PersonCreationRequest request3 = random(PersonCreationRequest.class);
        final PersonResponse response3 = personService.create(request3);

        personService.create(request3);
        personService.create(request3);
        personService.create(request3);
        personService.create(request3);
        personService.create(request3);
        personService.create(request3);

        final PersonSearchResponse response = personService.search(
                new PersonSearchRequest(response3.getName(), null,
                        new SortingAndPagingMetaData(SortField.NAME, "DESC", 1, 2))
        );

        assertNotNull(response);
        assertEquals(response.getPersons().size(), 2);
    }

}
