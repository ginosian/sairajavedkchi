package com.saira;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:com.saira/saira-context-web.xml")
public class SairaApplication {

    protected SairaApplication() {
        super();
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(SairaApplication.class)
                .logStartupInfo(true)
                .bannerMode(Banner.Mode.LOG)
                .run(args);
    }
}