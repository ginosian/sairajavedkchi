package com.saira.dto.person;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonUpdateDTO {
    private Long id;
    private String name;
}
