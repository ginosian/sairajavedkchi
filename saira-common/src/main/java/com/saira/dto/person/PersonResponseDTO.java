package com.saira.dto.person;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class PersonResponseDTO {
    private Long id;
    private String name;
    private Date creationDate;
}
