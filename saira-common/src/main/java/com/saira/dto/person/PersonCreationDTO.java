package com.saira.dto.person;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonCreationDTO {
    private String name;
}
