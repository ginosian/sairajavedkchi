package com.saira.enums;

public enum SortField {
    NAME("name"),
    CREATION_DATE("created");

    private String filedName;

    SortField(String filedName) {
        this.filedName = filedName;
    }

    public String getFiledName() {
        return filedName;
    }

    public static SortField fromString(final String stringValue) {
        return SortField.valueOf(stringValue);
    }
}
